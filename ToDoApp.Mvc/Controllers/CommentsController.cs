﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoApp.Mvc.DataAccess;
using ToDoApp.Mvc.Models;

namespace ToDoApp.Mvc.Controllers
{
    public class CommentsController : Controller
    {
        public ActionResult Index(int id)
        {
            CommentRepository repository = new CommentRepository();
            List<Comment> comments = repository.GetAll(c => c.NoteId == id);
            ViewBag.NoteId = id;

            return View(comments);
        }

        public ActionResult Create(int id)
        {
            Comment model = new Comment();
            model.NoteId = id;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Comment model)
        {
            model.CreatedAt = DateTime.Now;
            model.UpdatedAt = DateTime.Now;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CommentRepository repository = new CommentRepository();
            repository.Insert(model);

            return RedirectToAction("Index", new { id = model.NoteId });
        }

        public ActionResult Edit(int id)
        {
            CommentRepository repository = new CommentRepository();
            Comment model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Comment model)
        {
            model.UpdatedAt = DateTime.Now;

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CommentRepository repository = new CommentRepository();
            repository.Update(model);

            return RedirectToAction("Index", new { id = model.NoteId });
        }

        public ActionResult Delete(int id)
        {
            CommentRepository repository = new CommentRepository();
            Comment model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            ViewBag.NoteId = model.NoteId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Comment model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            CommentRepository repository = new CommentRepository();
            model = repository.Get(model.Id);
            int noteId = model.NoteId;

            repository.Delete(model);

            return RedirectToAction("Index", new { id = noteId} );
        }
    }
}