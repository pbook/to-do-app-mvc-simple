﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ToDoApp.Mvc.DataAccess;
using ToDoApp.Mvc.Models;

namespace ToDoApp.Mvc.Controllers
{
    public class NotesController : Controller
    {
        public ActionResult Index()
        {
            NoteRepository repository = new NoteRepository();
            List<Note> model = repository.GetAll();

            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Note model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NoteRepository repository = new NoteRepository();
            repository.Insert(model);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            NoteRepository repository = new NoteRepository();
            Note model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Note model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NoteRepository repository = new NoteRepository();
            repository.Update(model);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            NoteRepository repository = new NoteRepository();
            Note model = repository.Get(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Note model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            NoteRepository repository = new NoteRepository();
            model = repository.Get(model.Id);

            repository.Delete(model);

            return RedirectToAction("Index");
        }
    }
}