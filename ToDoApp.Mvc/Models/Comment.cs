﻿using System;

namespace ToDoApp.Mvc.Models
{
    public class Comment : BaseModel
    {
        public int NoteId { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public virtual Note Note { get; set; }
    }
}