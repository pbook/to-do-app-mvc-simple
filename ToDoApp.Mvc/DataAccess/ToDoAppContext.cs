﻿using System.Data.Entity;
using ToDoApp.Mvc.Models;

namespace ToDoApp.Mvc.DataAccess
{
    public class ToDoAppContext : DbContext
    {
        public ToDoAppContext() : base("ToDoAppDb")
        {

        }

        public DbSet<Note> Notes { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}