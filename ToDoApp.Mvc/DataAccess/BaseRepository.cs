﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using ToDoApp.Mvc.Models;

namespace ToDoApp.Mvc.DataAccess
{
    public abstract class BaseRepository<T> where T : BaseModel
    {
        private DbContext db;
        private DbSet<T> dbSet;

        public BaseRepository()
        {
            db = new ToDoAppContext();
            dbSet = db.Set<T>();
        }

        public List<T> GetAll()
        {
            return dbSet.ToList();
        }

        public List<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return dbSet.Where(filter).ToList();
        }

        public T Get(int id)
        {
            return dbSet.Find(id);
        }

        public T Get(Expression<Func<T,bool>> filter)
        {
            return dbSet.SingleOrDefault(filter);
        }

        public void Insert(T entity)
        {
            dbSet.Add(entity);
            db.SaveChanges();
        }

        public void Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Delete(T entity)
        {
            dbSet.Remove(entity);
            db.SaveChanges();
        }
    }
}